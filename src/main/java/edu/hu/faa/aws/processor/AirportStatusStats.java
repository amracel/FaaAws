package edu.hu.faa.aws.processor;

import edu.hu.faa.aws.model.AirportStatus;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Anne.Racel
 */
public class AirportStatusStats {
    
    private Map<String,Boolean> airportStatus;
    
    private Map<String,Integer> delayStatusCount;
    
    private List<String> delayReasons;
    
    /**
     * Initialize variables.
     */
    public AirportStatusStats() {
        airportStatus = new HashMap<String,Boolean>();
        delayStatusCount = new HashMap<String,Integer>();
        delayReasons = new ArrayList<String>();
    }
    
    /**
     * Add record to the queue. Create new record with key of IATA
     * and value of 1 if there is a delay at that airport.
     * @param status 
     */
    public void addAirportStatus(AirportStatus status) {
        airportStatus.put(status.getIata(), status.isDelay());
        
        if(!delayStatusCount.containsKey(status.getIata()))
            delayStatusCount.put(status.getIata(), new Integer(0));
        
        if(status.isDelay()) {
            Integer tmp = delayStatusCount.get(status.getIata());
            delayStatusCount.put(status.getIata(), tmp + 1);
            if(! status.getReason().isEmpty() && (!delayReasons.contains(status.getReason())))
                delayReasons.add(status.getReason());
                
        }
            
    }
    /**
     * Create the printable version of the stats
     * @return printable version.
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Airports Currently Delayed: ");
        for (Map.Entry<String, Boolean> entry : airportStatus.entrySet()) {
            if(entry.getValue().booleanValue())
                buffer.append("\t" + entry.getKey() + "\n");
        }
        buffer.append("\nCount of Delays: ");
        for (Map.Entry<String,Integer> entry: delayStatusCount.entrySet()) {
            buffer.append("\t" + entry.getKey() + ": " + entry.getValue().toString());
        }
        buffer.append("\nReasons for Delays: ");
        Iterator<String> it = delayReasons.iterator();
        while(it.hasNext()) {
            buffer.append("\t" + it.next());
        }
        
        return buffer.toString();
    }
}

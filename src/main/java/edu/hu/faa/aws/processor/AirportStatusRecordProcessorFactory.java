/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.hu.faa.aws.processor;

import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;

/**
 * Class to implement the RecordProcessorFactory in order to gather statistics.
 * @author Anne.Racel
 */
public class AirportStatusRecordProcessorFactory implements IRecordProcessorFactory{
      /**
     * Constructor.
     */
    public AirportStatusRecordProcessorFactory() {
        super();
    }
 public IRecordProcessor createProcessor() {
        return new AirportStatusRecordProcessor();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.hu.faa.aws.processor;

import com.amazonaws.services.kinesis.clientlibrary.exceptions.InvalidStateException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ShutdownException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ThrottlingException;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownReason;
import com.amazonaws.services.kinesis.model.Record;

import edu.hu.faa.aws.model.AirportStatus;

import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.parser.ParseException;

/**
 * Primary record manipulator.
 * @author Anne.Racel
 */
public class AirportStatusRecordProcessor implements IRecordProcessor {
    
    // Reporting interval
    private static final long REPORTING_INTERVAL_MILLIS = 60000L; // 1 minute
    private long nextReportingTimeInMillis;

    // Checkpointing interval
    private static final long CHECKPOINT_INTERVAL_MILLIS = 60000L; // 1 minute
    private long nextCheckpointTimeInMillis;
    
    private AirportStatusStats stats = new AirportStatusStats();

    private static final Log LOG = LogFactory.getLog(AirportStatusRecordProcessor.class);
    private String kinesisShardId;

    /**
     * Set up the log. Set the reporting times. Save the shard (dataset) id.
     * @param shardId 
     */
    public void initialize(String shardId) {
        LOG.info("Initializing record processor for shard: " + shardId);
        this.kinesisShardId = shardId;
        nextReportingTimeInMillis = System.currentTimeMillis() + REPORTING_INTERVAL_MILLIS;
        nextCheckpointTimeInMillis = System.currentTimeMillis() + CHECKPOINT_INTERVAL_MILLIS;
    }

    /**
     * Read the list of incoming records and pass them for processing. Check the
     * times and see if we have reached a checkpoint or point for
     * reporting stats.
     * @param list
     * @param irpc 
     */
    public void processRecords(List<Record> list, IRecordProcessorCheckpointer irpc) {
        for (Record record : list) {
            // process record
            processRecord(record);
        }

        // If it is time to report stats as per the reporting interval, report stats
        if (System.currentTimeMillis() > nextReportingTimeInMillis) {
            reportStats();
            resetStats();
            nextReportingTimeInMillis = System.currentTimeMillis() + REPORTING_INTERVAL_MILLIS;
        }

        // Checkpoint once every checkpoint interval
        if (System.currentTimeMillis() > nextCheckpointTimeInMillis) {
            checkpoint(irpc);
            nextCheckpointTimeInMillis = System.currentTimeMillis() + CHECKPOINT_INTERVAL_MILLIS;
        }
    }

    /**
     * Notify the stats object.
     */
     private void reportStats() {
        System.out.println("****** Shard " + kinesisShardId + " stats for last 1 minute ******\n" +
                stats + "\n" +
                "****************************************************************\n");
    }
     /**
      * Reset the stats for the next cycle. Note: it appears that, after the
      * first time this is run, the first report is on data saved from the
      * previous run.
      */
     private void resetStats() {
         stats = new AirportStatusStats();
     }
     /**
      * Terminate the process
      * @param checkpointer
      * @param reason 
      */
    public void shutdown(IRecordProcessorCheckpointer checkpointer, ShutdownReason reason) {
        LOG.info("Shutting down record processor for shard: " + kinesisShardId);
        // Important to checkpoint after reaching end of shard, so we can start processing data from child shards.
        if (reason == ShutdownReason.TERMINATE) {
            checkpoint(checkpointer);
        }
    }
    /**
     * Convert the incoming data into an AirportStatus object
     * @param record incoming data
     */
    private void processRecord(Record record) {
       // AirportStatus status = AirportStatus.fromJsonAsBytes(record.getData().array());
    	try {
    	AirportStatus status = new AirportStatus(record.getData().array());
   
        stats.addAirportStatus(status);
    	}
    	catch (IOException io) {
    		LOG.error(io.getMessage());
    	} catch(ParseException pe) {
    		LOG.error(pe.getMessage());
    	}
    }
    /**
     * Save information for recovery in case of error.
     * @param checkpointer 
     */
    private void checkpoint(IRecordProcessorCheckpointer checkpointer) {
        LOG.info("Checkpointing shard " + kinesisShardId);
        try {
            checkpointer.checkpoint();
        } catch (ShutdownException se) {
            // Ignore checkpoint if the processor instance has been shutdown (fail over).
            LOG.info("Caught shutdown exception, skipping checkpoint.", se);
        } catch (ThrottlingException e) {
            // Skip checkpoint when throttled. In practice, consider a backoff and retry policy.
            LOG.error("Caught throttling exception, skipping checkpoint.", e);
        } catch (InvalidStateException e) {
            // This indicates an issue with the DynamoDB table (check for table, provisioned IOPS).
            LOG.error("Cannot save checkpoint to the DynamoDB table used by the Amazon Kinesis Client Library.", e);
        }
    }
    
}

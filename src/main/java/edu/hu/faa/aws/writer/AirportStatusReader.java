package edu.hu.faa.aws.writer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import edu.hu.faa.aws.model.AirportStatus;

/**
 * Calls the FAA delays web service. Converts the JSON string into an AirportStatus
 * object. Note: this isn't used currently. It was found better to just pass
 * the original string.
 * @author anne
 */
public class AirportStatusReader {

	private static int SLEEP = 1000;
	private List<String> airportCodes = new ArrayList<String>();
	private final String USER_AGENT = "Mozilla/5.0";
	private int currentCode = 0;
	private static final Log LOG = LogFactory.getLog(AirportStatusReader.class);

        /**
         * Initialize object by setting airport codes.
         */
	public AirportStatusReader() {
		airportCodes.add("SFO");
		airportCodes.add("BWI");
		airportCodes.add("MIA");
		airportCodes.add("ORD");
		airportCodes.add("ATL");
		airportCodes.add("LAX");
		airportCodes.add("BOS");
		airportCodes.add("STL");
		airportCodes.add("DFW");
		airportCodes.add("DEN");
		airportCodes.add("SEA");
		airportCodes.add("CLT");
		airportCodes.add("JFK");
	}

        /**
         * Call the FAA web service. If a good response (200) is received, 
         * pass the data to create an AirportStatus object.
         * @return 
         */
	public AirportStatus getNextStatus() {
		String code = airportCodes.get(getCurrentCode());
		String url = "http://services.faa.gov/airport/status/" + code
				+ "?format=application/json";
		StringBuffer nextStatus = new StringBuffer();
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();

			if (responseCode == 200) {

				BufferedReader br = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String tmp;
				while ((tmp = br.readLine()) != null) {
					nextStatus.append(tmp);
				}

			}
		} catch (MalformedURLException mx) {

		} catch (IOException ioex) {

		}
		return convertStatusToAirportObject(nextStatus.toString());
	}

        /**
         * Processing to convert JSON to AirportStatus object. Note: not all the
         * fields are implemented here.
         * @param jsonString
         * @return 
         */
	private AirportStatus convertStatusToAirportObject(String jsonString) {
		AirportStatus airportStatus = null;
		JSONParser parser = new JSONParser();
		List<Object> statuses = new ArrayList<Object>();
		try {
			JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
			JSONObject weatherObject = (JSONObject) jsonObject.get("weather");
			LOG.debug("weather:" + weatherObject.toString());
			JSONObject statusObject = (JSONObject) jsonObject.get("status");
			airportStatus = new AirportStatus(jsonObject.get("IATA").toString(),jsonObject.get("delay").toString());
			airportStatus.setOriginalString(jsonString);
			if(airportStatus.isDelay() && (!statusObject.get("reason").toString().isEmpty()))
				airportStatus.setReason(statusObject.get("reason").toString());
			if (weatherObject.get("temp") != null)
				airportStatus.setTemp(weatherObject.get("temp").toString());
			if (weatherObject.get("wind") != null)
				airportStatus.setWind(weatherObject.get("wind").toString());
			String avgDelay = statusObject.get("avgDelay").toString();
			if (!avgDelay.isEmpty()) {
				airportStatus.setAvgDelay(avgDelay);
			}

		} catch (ParseException pe) {
			LOG.error(pe.getMessage());
		}
		return airportStatus;
	}

        /**
         * Increment the counter for the current code. If we've reached the
         * end of the list, go back to the start.
         * @return current place for code
         */
	private int getCurrentCode() {
		if (currentCode == (airportCodes.size() - 1))
			currentCode = 0;
		else
			++currentCode;
		return currentCode;
	}
}

package edu.hu.faa.aws.writer;

import java.nio.ByteBuffer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.kinesis.model.DescribeStreamResult;
import com.amazonaws.services.kinesis.model.PutRecordRequest;
import com.amazonaws.services.kinesis.model.ResourceNotFoundException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.hu.faa.aws.model.AirportStatus;
import edu.hu.faa.aws.utils.ConfigurationUtils;
import edu.hu.faa.aws.utils.CredentialUtils;

/**
 * Sends the FAA web service JSON file through a datastream. Runs on Amazon Kinesis.
 * See: http://docs.aws.amazon.com/kinesis/latest/dev/kinesis-sample-application.html
 * @author anne
 */
public class AirportStatusWriter {

	private static final Log LOG = LogFactory.getLog(AirportStatusWriter.class);
	private final static ObjectMapper JSON = new ObjectMapper();
	static {
		JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

        /**
         * Check that the proper arguments have been passed when starting the code.
         * @param args Stream name, AWS region
         */
	private static void checkUsage(String[] args) {
		if (args.length != 2) {
			System.err.println("Usage: "
					+ AirportStatusWriter.class.getCanonicalName()
					+ " <stream name> <region>");
			System.exit(1);
		}
	}

	/**
	 * Checks if the stream exists and is active
	 *
	 * @param kinesisClient
	 *            Amazon Kinesis client instance
	 * @param streamName
	 *            Name of stream
	 */
	private static void validateStream(AmazonKinesis kinesisClient,
			String streamName) {
		try {
			DescribeStreamResult result = kinesisClient
					.describeStream(streamName);
			if (!"ACTIVE".equals(result.getStreamDescription()
					.getStreamStatus())) {
				System.err
						.println("Stream "
								+ streamName
								+ " is not active. Please wait a few moments and try again.");
				System.exit(1);
			}
		} catch (ResourceNotFoundException e) {
			System.err.println("Stream " + streamName
					+ " does not exist. Please create it in the console.");
			System.err.println(e);
			System.exit(1);
		} catch (Exception e) {
			System.err.println("Error found while describing the stream "
					+ streamName);
			System.err.println(e);
			System.exit(1);
		}
	}

	/**
	 * Uses the Kinesis client to send the airport status to the given stream.
	 *
	 * @param airportStatus
	 *            instance representing the current airport status
	 * @param kinesisClient
	 *            Amazon Kinesis client
	 * @param streamName
	 *            Name of stream
	 */
	private static void sendAirportStatus(AirportStatus airportStatus,
			AmazonKinesis kinesisClient, String streamName) {
		byte[] bytes = null;
		bytes = airportStatus.toJsonAsBytes();

		// The bytes could be null if there is an issue with the JSON
		// serialization by the Jackson JSON library.
		if (bytes == null) {
			LOG.warn("Could not get JSON bytes for airport status");
			return;
		}

		LOG.info("Putting status: " + airportStatus.getIata() + ": "
				+ airportStatus.isDelay());
		if (airportStatus.isDelay())
			LOG.info("\n\tReason for Delay: " + airportStatus.getReason());
		PutRecordRequest putRecord = new PutRecordRequest();
		putRecord.setStreamName(streamName);
		// Using the airport code as the key
		putRecord.setPartitionKey(airportStatus.getIata());
		putRecord.setData(ByteBuffer.wrap(bytes));

		try {
			kinesisClient.putRecord(putRecord);
		} catch (AmazonClientException ex) {
			LOG.warn("Error sending record to Amazon Kinesis.", ex);
		}
	}

        /**
         * Confirm the correct arguments have been passed. Check the AWS credentials.
         * Validate that the stream is active. Send requests to FAA web service.
         * @param args
         * @throws Exception 
         */
	public static void main(String[] args) throws Exception {
		checkUsage(args);

		String streamName = args[0];
		String regionName = args[1];
		Region region = RegionUtils.getRegion(regionName);
		if (region == null) {
			System.err.println(regionName + " is not a valid AWS region.");
			System.exit(1);
		}

		AWSCredentials credentials = CredentialUtils.getCredentialsProvider()
				.getCredentials();

		AmazonKinesis kinesisClient = new AmazonKinesisClient(credentials,
				ConfigurationUtils.getClientConfigWithUserAgent());
		kinesisClient.setRegion(region);

		// Validate that the stream exists and is active
		validateStream(kinesisClient, streamName);

		// Repeatedly send stock trades with a 1000 milliseconds wait in between
		AirportStatusReader reader = new AirportStatusReader();
		while (true) {
			AirportStatus status = reader.getNextStatus();
			sendAirportStatus(status, kinesisClient, streamName);
			Thread.sleep(1000);
		}
	}

}

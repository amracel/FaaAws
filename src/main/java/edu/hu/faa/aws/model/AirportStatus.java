package edu.hu.faa.aws.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Calendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Object containing data for airport status. Much of this data is not
 * used in the current implementation. But all fields are covered for 
 * future development efforts.
 * @author Anne Racel <amracel@stuffandjunk.net>
 */
public class AirportStatus {

	private boolean delay;
	private String iata;
	private String state;
	private String name;
	private float visibility;
	private String weather;
	private Calendar updated;
	private String icao;
	private String city;
	private String reason;
	private int avgDelay;
	private String trend;
	private String typeDelay;
	private String wind;
	private float temp;
	private String originalString;

	private static final Log LOG = LogFactory.getLog(AirportStatus.class);

	private final static ObjectMapper JSON = new ObjectMapper();
	static {
		JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

        /**
         * Create generic object with no incoming data.
         */
	public AirportStatus() {
		setDelay("false");
		setIata("");
		setState("");
		setName("");
		setVisibility("0");
		setWeather("");
		setUpdated("0:00 AM");
		setIcao("");
		setCity("");
		setReason("");
		setAvgDelay("0");
		setTrend("");
		setTypeDelay("");
		setWind("");
		setTemp("0");

	}

        /**
         * Create object with basic info needed for processing
         * @param cityCode Airport IATA code
         * @param delayed  whether the flights at this airport are showing
         * delays.
         */
	public AirportStatus(String cityCode, String delayed) {
		setIata(cityCode);
		setDelay(delayed);
		setState("");
		setName("");
		setVisibility("0");
		setWeather("");
		setUpdated("0:00 AM");
		setIcao("");
		setCity("");
		setReason("");
		setAvgDelay("0");
		setTrend("");
		setTypeDelay("");
		setWind("");
		setTemp("0");
	}

	public AirportStatus(byte[] bytes) throws IOException, ParseException {
		fromJsonAsBytes(bytes);
	}

	public boolean isDelay() {
		return delay;
	}

	public void setDelay(String delay) {
		this.delay = (delay.equals("true"));
	}

	public String getIata() {
		return iata;
	}

	public void setIata(String iata) {
		this.iata = iata;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = Float.valueOf(visibility);
	}

	public String getWeather() {
		return weather;
	}

	public void setWeather(String weather) {
		this.weather = weather;
	}

	public Calendar getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		Calendar cal = Calendar.getInstance();
		String[] dateTokens = updated.split("\\s");
		String[] timeTokens = dateTokens[0].split(":");
		if (dateTokens[1].equals("PM")) {
			cal.set(Calendar.HOUR, Integer.parseInt(timeTokens[0]) + 12);
		} else {
			cal.set(Calendar.HOUR, Integer.parseInt(timeTokens[0]));
		}
		cal.set(Calendar.MINUTE, Integer.parseInt(timeTokens[1]));
		this.updated = cal;
	}

	public String getIcao() {
		return icao;
	}

	public void setIcao(String icao) {
		this.icao = icao;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getAvgDelay() {
		return avgDelay;
	}

	public void setAvgDelay(String avgDelay) {
		int tmpDelay = 0;
		String[] delayTokens = avgDelay.split("\\s");
		if (avgDelay.contains("and")) {
			if (delayTokens[1].equals("hours")) {
				tmpDelay = Integer.parseInt(delayTokens[0]) * 60;
			}
			if ((delayTokens.length >= 5) && isNumeric(delayTokens[3])) {
				tmpDelay = Integer.parseInt(delayTokens[3]);
			}
		} else {
			tmpDelay = Integer.parseInt(delayTokens[0]);
		}
		this.avgDelay = tmpDelay;
	}

	public String getTrend() {
		return trend;
	}

	public void setTrend(String trend) {
		this.trend = trend;
	}

	public String getTypeDelay() {
		return typeDelay;
	}

	public void setTypeDelay(String typeDelay) {
		this.typeDelay = typeDelay;
	}

	public String getWind() {
		return wind;
	}

	public void setWind(String wind) {
		this.wind = wind;
	}

	public float getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		String[] tempTokens = temp.split("\\s");
		this.temp = Float.parseFloat(tempTokens[0]);
	}

	public void setOriginalString(String json) {
		originalString = json;
	}

	public String getOriginalString() {
		return originalString;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + avgDelay;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + (delay ? 1231 : 1237);
		result = prime * result + ((iata == null) ? 0 : iata.hashCode());
		result = prime * result + ((icao == null) ? 0 : icao.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((reason == null) ? 0 : reason.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + Float.floatToIntBits(temp);
		result = prime * result + ((trend == null) ? 0 : trend.hashCode());
		result = prime * result
				+ ((typeDelay == null) ? 0 : typeDelay.hashCode());
		result = prime * result + ((updated == null) ? 0 : updated.hashCode());
		result = prime * result + Float.floatToIntBits(visibility);
		result = prime * result + ((weather == null) ? 0 : weather.hashCode());
		result = prime * result + ((wind == null) ? 0 : wind.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AirportStatus)) {
			return false;
		}
		AirportStatus other = (AirportStatus) obj;
		if (avgDelay != other.avgDelay) {
			return false;
		}
		if (city == null) {
			if (other.city != null) {
				return false;
			}
		} else if (!city.equals(other.city)) {
			return false;
		}
		if (delay != other.delay) {
			return false;
		}
		if (iata == null) {
			if (other.iata != null) {
				return false;
			}
		} else if (!iata.equals(other.iata)) {
			return false;
		}
		if (icao == null) {
			if (other.icao != null) {
				return false;
			}
		} else if (!icao.equals(other.icao)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (reason == null) {
			if (other.reason != null) {
				return false;
			}
		} else if (!reason.equals(other.reason)) {
			return false;
		}
		if (state == null) {
			if (other.state != null) {
				return false;
			}
		} else if (!state.equals(other.state)) {
			return false;
		}
		if (Float.floatToIntBits(temp) != Float.floatToIntBits(other.temp)) {
			return false;
		}
		if (trend == null) {
			if (other.trend != null) {
				return false;
			}
		} else if (!trend.equals(other.trend)) {
			return false;
		}
		if (typeDelay == null) {
			if (other.typeDelay != null) {
				return false;
			}
		} else if (!typeDelay.equals(other.typeDelay)) {
			return false;
		}
		if (updated == null) {
			if (other.updated != null) {
				return false;
			}
		} else if (!updated.equals(other.updated)) {
			return false;
		}
		if (Float.floatToIntBits(visibility) != Float
				.floatToIntBits(other.visibility)) {
			return false;
		}
		if (weather == null) {
			if (other.weather != null) {
				return false;
			}
		} else if (!weather.equals(other.weather)) {
			return false;
		}
		if (wind == null) {
			if (other.wind != null) {
				return false;
			}
		} else if (!wind.equals(other.wind)) {
			return false;
		}
		return true;
	}

	public static boolean isNumeric(String str) {
		return str.matches("-?\\d+(\\.\\d+)?"); // match a number with optional
												// '-' and decimal.
	}

	public void fromJsonAsBytes(byte[] bytes) throws IOException,
			ParseException {
		JSONParser parser = new JSONParser();

 		JsonNode node = JSON.readTree(bytes);
		JSONObject jsonObject = (JSONObject) parser.parse(node.asText());
		JSONObject weatherObject = (JSONObject) jsonObject.get("weather");
		LOG.debug("weather:" + weatherObject.toString());
		JSONObject statusObject = (JSONObject) jsonObject.get("status");
		setIata(jsonObject.get("IATA").toString());
		setDelay(jsonObject.get("delay").toString());
		if (isDelay() && (!statusObject.get("reason").toString().isEmpty()))
			setReason(statusObject.get("reason").toString());
		if (weatherObject.get("temp") != null)
			setTemp(weatherObject.get("temp").toString());
		if (weatherObject.get("wind") != null)
			setWind(weatherObject.get("wind").toString());
		String avgDelay = statusObject.get("avgDelay").toString();
		if (!avgDelay.isEmpty()) {
			setAvgDelay(avgDelay);
		}

	}

	public byte[] toJsonAsBytes() {
		try {
			return JSON.writeValueAsBytes(originalString);
		} catch (IOException e) {
			return null;
		}
	}
}
